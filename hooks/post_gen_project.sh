#!/usr/bin/env bash

set -e
python{{cookiecutter.python_version}} -mvenv venv
source venv/bin/activate
pip install -U pip
pip install -e .


echo -e "\033[1m\033[4mNext Steps\033[0m"
echo -e "\033[32mcd {{cookiecutter.project_name}}\033[0m"
echo -e "\033[32msource venv/bin/activate\033[0m"
echo -e "\033[32m{{cookiecutter.cli_name}} --help\033[0m"
