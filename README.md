# cookiecutter-python-click-light

This is a lightweight template for click-based python projects.

It is similar to and inspired by [@patdabaru's project](https://github.com/patdaburu/cookiecutter-click.git), but with fewer dependencies and fewer features. The precise use case for this template is when you have a quick-and-dirty script (that may not even be factored into a decent library) that you want to wrap in a command line interface without very much fuss.

It uses `setup.py` for ease of install/distribution.

If you want unit tests, documentation, continuous integration, version bumping, PyPi uploads, etc., do not bother trying to add that to this cookiecutter. Either use [@patdabaru's project](https://github.com/patdaburu/cookiecutter-click.git) if you just want a little documentation, or go all the way and use [@ionelmc's very sophisticated](https://github.com/ionelmc/cookiecutter-pylibrary) template that offers granular control over everything you could possibly want as you package your library/tool for distribution.

## Requirements

`cookiecutter` must be installed and on the path.

```
pip3 install --user cookiecutter
```

## Usage

```
cookiecutter https://gitlab.com/geoffbeier/cookiecutter-python-click-light.git
```
