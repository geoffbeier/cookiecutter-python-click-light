#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
.. currentmodule:: {{cookiecutter.package_name}}.cli
.. moduleauthor:: {{cookiecutter.author_name}} <{{cookiecutter.author_email}}>
This is the entry point for the command-line interface (CLI) application.  It
can be used as a handy facility for running the task from a command line.
.. note::
    To learn more about Click visit the
    `project website <http://click.pocoo.org/5/>`_.  There is also a very
    helpful `tutorial video <https://www.youtube.com/watch?v=kNke39OZ2k0>`_.
    To learn more about running Luigi, visit the Luigi project's
    `Read-The-Docs <http://luigi.readthedocs.io/en/stable/>`_ page.
"""

from loguru import logger
import click
from click_option_group import optgroup, MutuallyExclusiveOptionGroup
from .__init__ import __version__
import sys

class Info(object):
    """
    This is an information object that can be used to pass data between CLI functions.
    """
    def __init__(self):  # Note that this object must have an empty constructor.
        self.verbose: int = 0


# pass_info is a decorator for functions that pass 'Info' objects.
#: pylint: disable=invalid-name
pass_info = click.make_pass_decorator(
    Info,
    ensure=True
)


# Change the options to below to suit the actual options for your task (or
# tasks).
@click.group()
@optgroup.group('Output Verbosity', cls=MutuallyExclusiveOptionGroup, help="(mutually exclusive)")
@optgroup.option('--verbose', '-v', count=True, help="Enable verbose output. (Repeat multiple times to increase verbosity.)")
@optgroup.option('--quiet', '-q', count=True, help="Only print error output.")
@pass_info
def cli(info: Info,
        quiet: int,
        verbose: int):
    """
    Run {{cookiecutter.cli_name}}.
    """
    level = 'WARNING'
    if quiet > 0:
        level = 'ERROR'
    elif verbose == 1:
        level = 'INFO'
    elif verbose >= 2:
        level = 'DEBUG'
    logger.remove()
    logger.add(sys.stderr, level=level)

    info.verbose = verbose

@cli.command()
@pass_info
def hello(_: Info):
    """
    Say 'hello' to the nice people.
    """
    click.echo(f"{{cookiecutter.cli_name}} says 'hello'")


@cli.command()
def version():
    """
    Get the library version.
    """
    click.echo(click.style(f'{__version__}', bold=True))
