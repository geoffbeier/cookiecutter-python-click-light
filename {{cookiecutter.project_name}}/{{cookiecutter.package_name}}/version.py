#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
.. currentmodule:: {{cookiecutter.package_name}}.version
.. moduleauthor:: {{cookiecutter.author_name}} <{{cookiecutter.author_email}}>
This module contains project version information.
"""

__version__ = '{{cookiecutter.project_version}}'  #: the working version
__release__ = '{{cookiecutter.project_version}}'  #: the release version

